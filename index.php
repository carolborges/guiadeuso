<?php include("header.php");?>

      <div class="containerTudo busca-destaque">
        <div class="container-fluid">
          <div class="row-fluid">
            <section>
              <header><h1>O que você deseja saber?</h1></header>
              <article>
                <div class="box-busca-destaque">
                  <form action="#">
                    <input type="text" placeholder="pesquisar nos tópicos de ajuda">
                    <button>buscar</button>
                  </form>
                </div>
              </article>
            </section>
          </div>
        </div>
      </div>

      <div class="containerTudo topicos-novidade">
        <div class="container-fluid">
          <div class="row-fluid">
            <section>
              <header><h1>Posts Novidades</h1></header>
              <article>
                <ul>
                  <li>
                    <p>
                      <a href="leitura.php">
                        <span>
                          <img src="img/icon-categoria-large.png">
                        </span> 
                        Nome da categoria
                      </a>
                    </p>
                    <h1><a href="leitura.php">Automatizações de Monitor e Tickets</a></h1>
                  </li>
                  <li>
                    <p>
                      <a href="leitura.php">
                        <span>
                          <img src="img/icon-categoria-large.png">
                        </span> 
                        Nome da categoria
                      </a>
                    </p>
                    <h1><a href="leitura.php">Automatizações de Monitor e Tickets</a></h1>
                  </li>
                  <li>
                    <p>
                      <a href="leitura.php">
                        <span>
                          <img src="img/icon-categoria-large.png">
                        </span> 
                        Nome da categoria
                      </a>
                    </p>
                    <h1><a href="leitura.php">Automatizações de Monitor e Tickets</a></h1>
                  </li>
                  <li>
                    <p>
                      <a href="leitura.php">
                        <span>
                          <img src="img/icon-categoria-large.png">
                        </span> 
                        Nome da categoria
                      </a>
                    </p>
                    <h1><a href="leitura.php">Automatizações de Monitor e Tickets</a></h1>
                  </li>
                </ul>
              </article>
            </section>
          </div>
        </div>
      </div>

      <div class="containerTudo categorias-destaque">
        <div class="container-fluid">
          <div class="row-fluid">
            <section>
              <header><h1>Categorias Destaques</h1></header>
              <article>
                <ul>
                  <li>
                    <a href="categoria.php"><img src="img/icon-categoria-large.png"></a>
                    <h1><a href="categoria.php">Por dentro do Hugme</a></h1>
                    <a href="categoria.php">7 tópicos</a>
                  </li>
                  <li>
                    <a href="categoria.php"><img src="img/icon-categoria-large.png"></a>
                    <h1><a href="categoria.php">Por dentro do Hugme</a></h1>
                    <a href="categoria.php">7 tópicos</a>
                  </li>
                  <li>
                    <a href="categoria.php"><img src="img/icon-categoria-large.png"></a>
                    <h1><a href="categoria.php">Por dentro do Hugme</a></h1>
                    <a href="categoria.php">7 tópicos</a>
                  </li>
                </ul>
              </article>
            </section>
          </div>
        </div>
      </div>

<?php include("footer.php");?>