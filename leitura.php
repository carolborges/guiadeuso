<?php include("header.php");?>

      <div class="containerTudo leitura">
        <div class="container-fluid">
          <div class="row-fluid">
            <div class="box-cotegorias-leitura">
              <div class="box-busca-leitura">
                <form action="">
                  <input type="text" placeholder="buscar">
                  <button><img src="img/ico-busca-cinza@2x.png" alt=""></button>
                </form>
              </div>
              <ul class="lista-categorias-leitura">
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Primeiros passos</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Pode dentro do Hugme</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Primeiros passos</span>
                  </a>
                  <ul>
                    <li><a href="#">Segundo nível</a></li>
                    <li><a href="#">Segundo nível</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Pode dentro do Hugme</span>
                  </a>
                </li>
              </ul>
            </div>
            <section>  
              <header>
                <div class="migalha">
                  <ul>
                    <li>
                      <a href="#">central de ajuda</a>
                    </li>
                    <li>
                      <a href="#">primeiro nível</a>
                    </li>
                    <li>
                      <span href="#">segundo nível</span>
                    </li>
                  </ul>
                </div>
                <a href="#" class="print"><img src="img/print.png" alt=""></a>
                <h1>Visão Geral</h1>
                <p>atualizado em: 23/05/2014 às 23h45</p>
              </header>
              <article>
                <h2>Acesso</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh <a href="#">euismod tincidunt</a> ut laoreet dolore magna aliquam erat volutpat.</p>
                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

                <h3>Painel</h3>
                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                <div class="box-video-leitura">
                  <iframe width="280" height="158" src="//www.youtube.com/embed/FyS9GA3Pe3U?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat <a href="#">Painel</a>, <a href="#">Monitor</a>, <a href="#">Social CRM</a>, <a href="#">Mensagens</a>, <a href="#">Dados</a> e <a href="#">Ajustes</a>.</p>
                <img src="img/monitor.jpg">
                <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                <blockquote>
                  <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                  <p>Ut wisi enim ad minim veniam, <a href="#">quis nostrud exerci</a> tation ullamcorper suscipit.</p>
                </blockquote>
              </article>
              <div class="ajudou">
                <h1>Isso ajudou você?</h1>
                <div class="box-resposta-ajudou">
                  <a href="javascript:void(false);" id="aprovado" class="sim">sim</a>
                  <a href="javascript:void(false);" id="reprovado" class="nao">não</a>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>

<?php include("footer.php");?>

<script>
  $( "#aprovado" ).click(function() {
    $( this ).toggleClass( "active-sim" );
    $( "#reprovado" ).removeClass( "active-nao" );
  });
  $( "#reprovado" ).click(function() {
    $( this ).toggleClass( "active-nao" );
    $( "#aprovado" ).removeClass( "active-sim" );
  });
</script>