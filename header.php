<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Style-Type" content="text/css">

    <meta name="description" content="Descrição sobre qoe é o site">
    <meta name="keywords" content="Palavras-chave do site">
    <meta property="og:image" content="http://www.enderecodaimg.com.br"/>
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Autor">
    <meta name="copyright" content="Autor">
    <base href=".">
    
    <title>Hugme - Guia de Uso</title>  


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link href="css/estrutura.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <link rel="canonical" href="http://www.endereco-do-site.com.br" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <div id="wrap">
      <div class="box-menu-lateral slideout-menu">
        <ul class="list-pag">
          <li><a href="#" class="fecha-menu slideout-menu-toggle"><img src="img/ico-nav@2x.png" width="35" height="23"></a></li>
          <li><a href="categoria.php">Central de ajuda</a></li>
          <li><a href="contato.php">Fale com a gente</a></li>
          <li><a href="www.reclameaqui.com.br/cursos" target="_blank">Treinamentos</a></li>
        </ul>
        <ul class="lista-categorias-leitura">
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Primeiros passos</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Primeiros passos</span>
            </a>
            <ul>
              <li><a href="#">Segundo nível</a></li>
              <li><a href="#">Segundo nível</a></li>
            </ul>
          </li>
          <li>
            <a href="#">
              <img src="img/icon-categoria-large.png">
              <span>Pode dentro do Hugme</span>
            </a>
          </li>
        </ul>
      </div>
      <div class="box-busca-lateral slideout-busca">
        <div class="campo-busca">
          <div class="box-campo-busca">
            <div class="box-input-busca-topo">
              <form action="#">
                  <input type="text" placeholder="buscar">
                  <button><img src="img/ico-busca-cinza@2x.png" alt=""></button>
              </form>
            </div>
            <a href="javascript:void(0);" class="cancelar-busca slideout-busca-toggle">cancelar</a>
          </div>
        </div>
      </div>
      <header>
        <div class="containerTudo topo">
          <div class="container-fluid">
            <div class="row-fluid">
              <nav class="menu-mobile">
                <a href="#" class="slideout-menu-toggle">
                  <img src="img/ico-nav.png">
                </a>
              </nav>
              <div class="busca-topo slideout-busca-toggle">
                <a href="#">
                  <img src="img/ico-busca.png">
                </a>
              </div>
              <h1><a href="#"><img src="img/logo-hugme.png" alt="Hugme"></a></h1>
              <nav class="menu-desk">
                <ul>
                  <li><a href="categoria.php">central de ajuda</a></li>
                  <li><a href="http://www.reclameaqui.com.br/cursos" target="_blank">treinamentos</a></li>
                  <li><a href="contato.php">fale com a gente</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>