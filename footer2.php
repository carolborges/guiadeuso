     <div id="push"></div>
    </div>

    <div id="footer">
      <footer class="container-fluid">
        <p class="muted credit">© 2015 Hugme - Todos os direitos reservados.</p>
      </footer>
    </div>

    <script src="js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="js/retina.js"></script>

        <script type="text/javascript">
      $(document).ready(function () {
          $('.slideout-menu-toggle').on('click', function(event){
            event.preventDefault();
            // create menu variables
            var slideoutMenu = $('.slideout-menu');
            var slideoutMenuWidth = $('.slideout-menu').width();
            
            // toggle open class
            slideoutMenu.toggleClass("open");
            
            // slide menu
            if (slideoutMenu.hasClass("open")) {
              slideoutMenu.animate({
                left: "0px"
              }); 
            } else {
              slideoutMenu.animate({
                left: -slideoutMenuWidth
              }, 250);  
            }
          });
      });

      $(document).ready(function () {
          $('.slideout-busca-toggle').on('click', function(event){
            event.preventDefault();
            // create menu variables
            var slideoutMenu = $('.slideout-busca');
            var slideoutMenuWidth = $('.slideout-busca').width();
            
            // toggle open class
            slideoutMenu.toggleClass("open");
            
            // slide menu
            if (slideoutMenu.hasClass("open")) {
              slideoutMenu.animate({
                right: "0px"
              }); 
            } else {
              slideoutMenu.animate({
                right: -slideoutMenuWidth
              }, 250);  
            }
          });
      });
    </script>
  </body>
</html>