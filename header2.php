<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Style-Type" content="text/css">

    <meta name="description" content="Descrição sobre qoe é o site">
    <meta name="keywords" content="Palavras-chave do site">
    <meta property="og:image" content="http://www.enderecodaimg.com.br"/>
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Autor">
    <meta name="copyright" content="Autor">
    <base href=".">
    
    <title>Hugme - Guia de Uso</title>  


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link href="css/estrutura.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <link rel="canonical" href="http://www.endereco-do-site.com.br" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <div id="wrap">
      <header>
        <div class="containerTudo topo login">
          <div class="container-fluid">
            <div class="row-fluid">
              <h1><a href="#"><img src="img/logo-hugme.png" alt="Hugme"></a></h1>
            </div>
          </div>
        </div>
      </header>