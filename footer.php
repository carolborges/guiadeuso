      <div class="containerTudo rodape">
        <div class="container-fluid">
          <div class="row-fluid">
            <section>
              <header><h1>Destaques Rodapé</h1></header>
              <article>
                <div class="centralajuda">
                  <h1>Central de Ajuda</h1>
                  <ul>
                    <li><a href="#">Primeiros passos</a></li>
                    <li><a href="#">Pode dentro do Hugme</a></li>
                    <li><a href="#">Configurando o Hugme</a></li>
                    <li><a href="#">Dúvidas frequêntes</a></li>
                    <li><a href="#">Glossário</a></li>
                  </ul>
                </div>
              </article>
              <article>
                <div class="duvidas-sugestoes">
                  <h1>Dúvidas ou sugestão?</h1>
                  <h2>fale com o nosso time</h2>
                  <ul>
                    <li>
                      <img src="img/atendente.png" alt="Pernalonga">
                      <span>Aline</span>
                    </li>
                    <li>
                      <img src="img/atendente.png" alt="Pernalonga">
                      <span>Gisele</span>
                    </li>
                    <li>
                      <img src="img/atendente.png" alt="Pernalonga">
                      <span>Denise</span>
                    </li>
                  </ul>
                  <a href="contato.php" class="contato-footer">fale com a gente</a>
                </div>
              </article>
              <article>
                <div class="treinamentos">
                  <h1>Nós treinamos seu time!</h1>   
                  <small>saiba mais sobre a capacitação de equipes</small>
                  <a href="http://www.reclameaqui.com.br/cursos" target="_blank">saiba mais</a>
                </div>
              </article>
            </section>
          </div>
        </div>
      </div>

      <div id="push"></div>
    </div>

    <div id="footer">
      <footer class="container-fluid">
        <p class="muted credit">© 2015 Hugme - Todos os direitos reservados.</p>
      </footer>
    </div>

    <script src="js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="js/retina.js"></script>

    <script type="text/javascript">
      $(document).ready(function () {
          $('.slideout-menu-toggle').on('click', function(event){
            event.preventDefault();
            // create menu variables
            var slideoutMenu = $('.slideout-menu');
            var slideoutMenuWidth = $('.slideout-menu').width();
            
            // toggle open class
            slideoutMenu.toggleClass("open");
            
            // slide menu
            if (slideoutMenu.hasClass("open")) {
              slideoutMenu.animate({
                left: "0px"
              }); 
            } else {
              slideoutMenu.animate({
                left: -slideoutMenuWidth
              }, 250);  
            }
          });
      });

      $(document).ready(function () {
          $('.slideout-busca-toggle').on('click', function(event){
            event.preventDefault();
            // create menu variables
            var slideoutMenu = $('.slideout-busca');
            var slideoutMenuWidth = $('.slideout-busca').width();
            
            // toggle open class
            slideoutMenu.toggleClass("open");
            
            // slide menu
            if (slideoutMenu.hasClass("open")) {
              slideoutMenu.animate({
                right: "0px"
              }); 
            } else {
              slideoutMenu.animate({
                right: -slideoutMenuWidth
              }, 250);  
            }
          });
      });
    </script>
  </body>
</html>