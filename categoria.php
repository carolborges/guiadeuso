<?php include("header.php");?>

      <div class="containerTudo categorias">
        <div class="container-fluid">
          <div class="row-fluid">
            <div class="box-cotegorias-leitura">
              <div class="box-busca-leitura">
                <form action="">
                  <input type="text" placeholder="buscar">
                  <button><img src="img/ico-busca-cinza@2x.png" alt=""></button>
                </form>
              </div>
              <ul class="lista-categorias-leitura">
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Primeiros passos</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Pode dentro do Hugme</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Primeiros passos</span>
                  </a>
                  <ul>
                    <li><a href="#">Segundo nível</a></li>
                    <li><a href="#">Segundo nível</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#">
                    <img src="img/icon-categoria-large.png">
                    <span>Pode dentro do Hugme</span>
                  </a>
                </li>
              </ul>
            </div>
            <section>
              <header>
                <div class="migalha">
                  <ul>
                    <li>
                      <a href="#">central de ajuda</a>
                    </li>
                    <li>
                      <a href="#">primeiro nível</a>
                    </li>
                    <li>
                      <span href="#">segundo nível</span>
                    </li>
                  </ul>
                </div>
                <h1>Primeiros passos</h1>
              </header>
              <article>
                <ul>
                  <li><a href="#">Visão Geral</a></li>
                  <li><a href="#">Navegação</a></li>
                  <li><a href="#">Tipos de usuário</a></li>
                  <li><a href="#">Canais de atendimento Hugme</a></li>
                  <li><a href="#">Monitorando Mídias Sociais</a></li>
                  <li><a href="#">Atendendo seus tickets</a></li>
                  <li><a href="#">Entenda a relação entre o Monitor e Tickets</a></li>
                  <li><a href="#">Automatizações</a></li>
                  <li><a href="#">Ciclo de vida de um item monitorado</a></li>
                  <li><a href="#">Ciclo de vida de um ticket</a></li>
                </ul>
              </article>
            </section>
          </div>
        </div>
      </div>

 <?php include("footer2.php");?>